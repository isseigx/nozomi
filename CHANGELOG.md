## Nozomi 1.3.0
### Fixes
-  Enter or exit of full screen mode does not work well in some window managers
- Memory leaks
- Stop playback on exit
- Clean up and refactor code
- Deprecations
### New
- Controls in full screen mode are aligned at the bottom
- Add recent files menu (Qt5)
- Use default icon size in full screen controls
- In the playlist view, the current played or paused files are marked with an icon
- Exit from full screen mode when last file playback finished
- Application can be optionally compiled with Qt6 (experimental and maybe removed in v1.4.0)

## Nozomi 1.2.0
### Fixes
- Drag and drop M4A, WMA, FLAC and Opus files into playlist view
- Add more fallback icons
- Full screen controls are resized and positioned as expected in multi-monitor configs
- Time labels are cleared propertly
- Open audio files in file dialog works well now
- In full screen mode, pressing play button starts playback from beginning of playlist
- Code clean up

### New
- It is posible to raise window and toggle full screen mode with player controls supporting these MPRIS2 methods
- Disable some actions like, play, next, previous or full screen when nothing is playing
- When previous action is triggered, restart current file instead of play previous if playing time is more than 5 seconds 
- Toggle full screen with double click on video

## Nozomi 1.1.0
### Fixes
- Do not show controls when they are created in some desktops (e.g. i3)
- Do not tray to load media when playlist is empty
- When enter in full screen mode do not show controls if cursor is hidden
- Exit full screen mode is handled by a QAction
- Remove unused code
### New
- Implement libVLC audio delay
- Implement libVLC subtitles delay
- New application icon
- Add new default icons and use them as theme fallback

## Nozomi 1.0.0
First major version with important amount of changes.
Most important is the use of libVLC instead of QtAV, this improve the subtitle handle and audio quality.

### Fixes
- ASS subtitles pick embed fonts by default (libVLC)
- Improved audio quality (libVLC)
- Playlist use default QAbstractItemModel functions to handle drag and drop
- Playlist handle dropped file urls

### New
- Floating controls in fullscreen mode
- Click on seek slider change video position
- Interface can handle more video file types
- Application handle audio files
- Actions in PlaylistView moved to context menu
- Remove qmake support

## v0.10.1
- Fix: crash when playlist items are moved
- Add stop button
- Disable subtitle tracks sub menu when there are no subtitles
- Remove unused code

## v0.10

- Fix: disable screensaver works under Wayland
- Fix: annoying error messages from QtDBus
- The video output setting is only displayed on Wayland sessions
- When open a file the directory is remembered
- Basic open url option
- GFX is used as display method under X11 window managers and DRM under Wayland compositors
- Performance improvements (Audio and subtitles tracks selection, playlist widget)