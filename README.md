## Nozomi

A simple video player for the linux desktop using [Qt](https://qt.io) and [libVLC](https://www.videolan.org/vlc/libvlc.html).

### Features

- Text and image based subtitles
- Multiple audio tracks
- Crop output and set aspect ratio
- Video snapshots
- Playlist
- MPRIS support
- Video and audio files playback

### Building & Installing

#### Dependencies
- Build Essential (debian and derivates)
- CMake
- Qt5 (ubuntu: `qttools5-dev`, arch: `qt5-tools`)
- VLC
- KIdleTime (ubuntu: `libkf5idletime-dev`, arch: `kidletime`)

#### Build

```
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install
```

### License

GPL v3 +


### Screenshots

![](https://i.imgur.com/udrNpnH.png)

![](https://i.imgur.com/0Lll22q.png)

![](https://i.imgur.com/EsjZwUf.png)
