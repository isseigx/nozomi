#include "mimechecker.h"
#include "playlist.h"

#include <QDebug>
#include <QIcon>
#include <QMimeData>
#include <QUrl>

Playlist::Playlist(Player *p, QObject *parent) : QAbstractListModel(parent),
mPlayer(p)
{
    connectToSignals();
}

QVariant Playlist::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= mItems.size()) {
        return QVariant();
    }
    
    const QUrl &item = mItems.at(index.row());

    if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
        return item.fileName();
    }

    if (index == mCurrentIndex && role == Qt::DecorationRole) {
        switch (mPlayer->state()) {
            case Player::Playing:
                return QIcon::fromTheme("media-playback-start", QIcon(":/play.svg"));
            case Player::Paused:
                return QIcon::fromTheme("media-playback-pause", QIcon(":/pause.svg"));
            default:
                return QIcon();
        }
    }
    
    if (role == Qt::UserRole) {
        return mItems.at(index.row());
    }
    
    return QVariant();
}

bool Playlist::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() >= 0 && index.row() < mItems.size()
            && role == Qt::UserRole) {
        const auto url = value.toUrl();
        
        if (mItems.at(index.row()) == url) {
            return true;
        }
        
        mItems.replace(index.row(), url);
        emit dataChanged(index, index, {Qt::DisplayRole, Qt::UserRole});
        return true;
    }
    
    return false;
}

int Playlist::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : mItems.size();
}

Qt::DropActions Playlist::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

Qt::ItemFlags Playlist::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;
    }
    
    return QAbstractListModel::flags(index) | Qt::ItemIsDragEnabled;
}

QStringList Playlist::mimeTypes() const
{
    return QStringList(QStringLiteral("text/uri-list"));
}

QMimeData *Playlist::mimeData(const QModelIndexList &indexes) const
{
    QList<QUrl> urls;
    
    for (const QModelIndex &index : qAsConst(indexes)) {
        urls << index.data(Qt::UserRole).toUrl();
    }
    
    auto data = new QMimeData();
    data->setUrls(urls);
    
    return data;
}

bool Playlist::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row,
    int column, const QModelIndex &parent) const
{
    Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(parent);
    Q_UNUSED(column);
    
    if (data->hasUrls()) {
        return true;
    }
    return false;
}

bool Playlist::dropMimeData(const QMimeData *data, Qt::DropAction action, int row,
                               int column, const QModelIndex &parent)
{
    if (!canDropMimeData(data, action, row, column, parent)) {
        return false;
    }
    
    if (action == Qt::IgnoreAction) {
        return false;
    }
    
    int endRow;
    
    if (!parent.isValid()) {
        if (row < 0) {
            endRow = mItems.size();
        }
        else {
            endRow = qMin(row, mItems.size());
        }
    }
    else {
        endRow = parent.row();
    }
    
    const auto urls = data->urls();
    
    if (action == Qt::CopyAction) {
        const auto mimeTypes = MimeChecker::mimeTypes();
        
        for (const QUrl &url : qAsConst(urls)) {
            if (MimeChecker::isSupported(url.toLocalFile(), mimeTypes)) {
                beginInsertRows({}, endRow, endRow);
                mItems.insert(endRow, url);
                endInsertRows();
                
                ++endRow;
            }
        }
        
        return true;
    }
    
    if (action == Qt::MoveAction) {
        for (const QUrl &url : qAsConst(urls)) {
            beginInsertRows(QModelIndex(), endRow, endRow);
            mItems.insert(endRow, url);
            endInsertRows();
            
            ++endRow;
        }
        
        return true;
    }
        
    return QAbstractListModel::dropMimeData(data, action, row, column, parent);
}

bool Playlist::insertRows(int row, int count, const QModelIndex &parent)
{
    if (count < 1 || row < 0 || row > rowCount(parent)) {
        return false;
    }
    
    beginInsertRows(QModelIndex(), row, row + count - 1);
    
    for (auto r = 0; r < count; ++r) {
        mItems.insert(row, QUrl());
    }
    
    endInsertRows();
    
    return true;
}

bool Playlist::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count <= 0 || row < 0 || (row + count) > rowCount(parent)) {
        return false;
    }
    
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    
    const auto it = mItems.begin() + row;
    mItems.erase(it, it + count);
    
    endRemoveRows();
    
    return true;
}

bool Playlist::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild)
{
    if (sourceRow < 0
        || sourceRow + count - 1 >= rowCount(sourceParent)
        || destinationChild < 0
        || destinationChild > rowCount(destinationParent)
        || sourceRow == destinationChild
        || sourceRow == destinationChild - 1
        || count <= 0
        || sourceParent.isValid()
        || destinationParent.isValid()) {
        return false;
    }
    
    if (!beginMoveRows(QModelIndex(), sourceRow, sourceRow + count - 1, QModelIndex(), destinationChild)) {
        return false;
    }
    
    int fromRow = sourceRow;
    if (destinationChild < sourceRow) {
        fromRow += count - 1;
    }
    else {
        destinationChild--;
    }
    
    while (count--) {
        mItems.move(fromRow, destinationChild);
    }
    
    endMoveRows();
    
    return true;
}

void Playlist::add(const QUrl &item)
{
    if (!insertRow(mItems.size(), {})) {
        qDebug() << "[Playlist]: Cannot add item" << item.fileName();
        return;
    }

    const QModelIndex newIndex = index(mItems.size() - 1);
    setData(newIndex, item, Qt::UserRole);
}

void Playlist::connectToSignals()
{
    connect(mPlayer, &Player::mediaStateChanged, this, &Playlist::onMediaStateChanged);
    connect(mPlayer, &Player::stateChanged, this, &Playlist::onStateChanged);
}

void Playlist::onMediaStateChanged(Player::MediaState state)
{
    if (state != Player::EndReached) {
        return;
    }
    
    if (mCurrentIndex == index(mItems.size() - 1)) {
        emit finished();
    }
    else {
        next();
    }
}

void Playlist::onStateChanged(Player::PlayerState state)
{
    Q_UNUSED(state);
    emit dataChanged(mCurrentIndex, mCurrentIndex, {Qt::DecorationRole});
}

void Playlist::next()
{
    if (mItems.isEmpty()) {
        return;
    }

    if (!mCurrentIndex.isValid() || mCurrentIndex == index(mItems.size() - 1)) {
        mCurrentIndex = index(0);
    }
    else {
        mCurrentIndex = index(mCurrentIndex.row() + 1);
    }
    
    setMedia();
    
    emit currentIndexChanged(mCurrentIndex.row());
}

void Playlist::previous()
{
    /*
    * When playing time is more than 5 seconds, restart file playback
    * instead of play previous file
    */
    if (mPlayer->state() != Player::Stopped) {
        if (mPlayer->time() > 5000) {
            mPlayer->setTime(0);
            return;
        }
    }
    
    if(mItems.isEmpty()) {
        return;
    }

    if (!mCurrentIndex.isValid() || mCurrentIndex == index(0)) {
        mCurrentIndex = index(mItems.size() - 1);
    }
    else {
        mCurrentIndex = index(mCurrentIndex.row() - 1);
    }
    
    setMedia();
    
    emit currentIndexChanged(mCurrentIndex.row());
}

void Playlist::clear()
{
    removeRows(0, mItems.size(), {});
}

int Playlist::currentIndex() const
{
    return mCurrentIndex.row();
}

void Playlist::setCurrentIndex(int idx)
{
    if (idx < 0 || idx > rowCount({})) {
        return;
    }
    
    mCurrentIndex = index(idx);
    
    setMedia();
    
    emit currentIndexChanged(mCurrentIndex.row());
}

void Playlist::setMedia()
{
    const QUrl url = mCurrentIndex.data(Qt::UserRole).toUrl();
    
    if (mPlayer->state() == Player::Playing || mPlayer->state() == Player::Paused) {
        mPlayer->stop();
    }
    
    mPlayer->setMedia(url);
    mPlayer->play();
}
