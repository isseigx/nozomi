#include "player.h"

#include <QDebug>
#include <QUrl>

void event_callback(const struct libvlc_event_t *event, void *data)
{
    static_cast<Player*>(data)->onEventTriggered(event->type);
}

Player::Player(QObject *parent) : QObject(parent)
{
    setupObjects();
}

Player::~Player()
{
    libvlc_media_player_release(media_player);
    libvlc_media_list_player_release(list_player);
    libvlc_media_list_release(media_list);
    libvlc_release(vlc);
}

void Player::setupObjects()
{
    // init libvlc
    vlc = libvlc_new(0, nullptr);

    if (!vlc) {
        qCritical("[Player]: could not init libVLC");
        emit error(tr("Could not init audio engine"));
        return;
    }

    libvlc_set_app_id(vlc, "io.gitlab.isseigx.nozomi", NOZOMI_VERSION, "nozomi");

    media_list = libvlc_media_list_new(vlc);

    list_player = libvlc_media_list_player_new(vlc);
    libvlc_media_list_player_set_media_list(list_player, media_list);

    media_player = libvlc_media_list_player_get_media_player(list_player);
    libvlc_media_player_set_role(media_player, libvlc_role_Video);

    eventAttach();
}

int Player::currentIndex()
{
    libvlc_media_t *media = libvlc_media_player_get_media(media_player);

    if (!media)
        return -1;
    
    const int index = libvlc_media_list_index_of_item(media_list, media);
    libvlc_media_release(media);
    
    return index;
}

int Player::state()
{
    return static_cast<int>(libvlc_media_player_get_state(media_player));
}

float Player::currentPosition()
{
    return libvlc_media_player_get_position(media_player);
}

qint64 Player::currentDuration()
{
    libvlc_media_t *media = libvlc_media_player_get_media(media_player);
    if (!media)
        return -1;
    
    const qint64 dur = static_cast<qint64>(libvlc_media_get_duration(media));
    libvlc_media_release(media);
    return dur;
}

qint64 Player::time()
{
    return static_cast<qint64>(libvlc_media_player_get_time(media_player));
}

void Player::updateCurrentMedia()
{   
    // if player is not on playing state, we don't send any signal
    if (state() != 3)
        return;

    QMetaObject::invokeMethod(this, "currentIndexChanged", Qt::QueuedConnection, Q_ARG(int, currentIndex()));
    QMetaObject::invokeMethod(this, "durationChanged", Qt::QueuedConnection, Q_ARG(qint64, currentDuration()));
    
    const int subsCount = libvlc_video_get_spu_count(media_player);
    QMetaObject::invokeMethod(this, "subtitleAvailableChanged", Qt::QueuedConnection, Q_ARG(int, subsCount));
    
    const int audioTracks = libvlc_audio_get_track_count(media_player);
    QMetaObject::invokeMethod(this, "audioTracksAvailableChanged", Qt::QueuedConnection, Q_ARG(int, audioTracks));

    //TODO: extract thumbnail
}

void Player::play()
{
    //libvlc_state_t enum: libvlc_Playing = 3, libvlc_Paused = 4
    if (state() == 3) {
        libvlc_media_list_player_pause(list_player);
    }
    else {
        libvlc_media_list_player_play(list_player);
    }
}

void Player::playNow(const QString &file)
{
    addItem(file);
    const int index = itemsCount() - 1;
    setCurrentIndex(index);
}

void Player::stop()
{
    libvlc_media_list_player_stop(list_player);
}

void Player::next()
{
    libvlc_media_list_player_next(list_player);
    //updateCurrentMedia();
}

void Player::previous()
{
    libvlc_media_list_player_previous(list_player);
    //updateCurrentMedia();
}

void Player::clear()
{
    // if player's state is libvlc_Playing, stop it
    if (state() == 3) {
        libvlc_media_list_player_stop(list_player);
    }

    while (libvlc_media_list_count(media_list) != 0) {
        libvlc_media_list_remove_index(media_list, 0);
        itemRemoved(0);
    }
}

void Player::setCurrentIndex(int index)
{
    libvlc_media_list_player_play_item_at_index(list_player, index);
}

void Player::seek(float pos)
{
    if(!libvlc_media_player_is_seekable(media_player)) {
        return;
    }
    libvlc_media_player_set_position(media_player, pos);
    seeked((qint64) libvlc_media_player_get_time(media_player));
}

void Player::setTime(qint64 t)
{
    if (!libvlc_media_player_is_seekable(media_player)) return;

    libvlc_media_player_set_time(media_player, (libvlc_time_t) t);
    seeked(t);
}

void Player::addItems(const QStringList &items)
{
    for (const QString &item : items) {
        addItem(item);
    }
}

void Player::addItem(const QString &file)
{
    libvlc_media_list_lock(media_list);

    libvlc_media_list_add_media(media_list, libvlc_media_new_path(vlc,
    file.toUtf8().constData()));

    emit mediaAdded(file);
    emit itemsInserted(libvlc_media_list_count(media_list) - 1,
    libvlc_media_list_count(media_list));

    libvlc_media_list_unlock(media_list);
}

void Player::removeItem(int index)
{
    libvlc_media_list_lock(media_list);
    libvlc_media_list_remove_index(media_list, index);

    emit itemRemoved(index);

    libvlc_media_list_unlock(media_list);
}

void Player::insertItems(const QStringList &items, int position)
{
    const int count = itemsCount();
    if (count == 0) {
        addItems(items);
        return;
    }

    if (position == -1)
        position = count;

    libvlc_media_t *media;
    libvlc_media_list_lock(media_list);

    int oldpos = position;

    for (const QString &item : items) {
        media = libvlc_media_new_path(vlc, item.toUtf8().constData());
        libvlc_media_list_insert_media(media_list, media, position);
        libvlc_media_release(media);
        emit mediaInserted(position, item);
        ++position;
    }

    emit itemsInserted(oldpos, position);
    libvlc_media_list_unlock(media_list);
}

void Player::moveItem(int from, int to)
{
    libvlc_media_list_lock(media_list);
    libvlc_media_t *media = libvlc_media_list_item_at_index(media_list, from);

    const int cfrom = from;
    libvlc_media_list_insert_media(media_list, media, to);

    if (from > to) from += 1;

    libvlc_media_list_remove_index(media_list, from);
    libvlc_media_list_unlock(media_list);

    libvlc_media_release(media);
    emit itemMoved(cfrom, to);
}

int Player::itemsCount()
{
    libvlc_media_list_lock(media_list);
    const int count = libvlc_media_list_count(media_list);
    libvlc_media_list_unlock(media_list);

    return count;
}

void Player::addItems(const QVariant &item)
{
    QList<QUrl> urls = item.value<QList<QUrl>>();
    for (const QUrl &url : urls) {
        addItem(url.toString(QUrl::PreferLocalFile));
    }
}

bool Player::isSeekable()
{
    return libvlc_media_player_is_seekable(media_player);
}

QString Player::itemAt(int index)
{
    libvlc_media_list_lock(media_list);
    libvlc_media_t *media = libvlc_media_list_item_at_index(media_list, index);
    libvlc_media_list_unlock(media_list);

    if (!media) return QString();

    const QString mrl = QUrl::fromPercentEncoding(libvlc_media_get_mrl(media));
    libvlc_media_release(media);

    QUrl url(mrl);

    if (url.isLocalFile())
        return url.toString(QUrl::PreferLocalFile);

    return mrl;
}

void Player::setWindowOutput(uint32_t wid)
{
    libvlc_media_player_set_xwindow(media_player, wid);
}

void Player::setAspectRatio(AspectRatio ar)
{
    if (ar == RatioDefault) {
        libvlc_video_set_aspect_ratio(media_player, 0);
    } else if (ar == Ratio4_3) {
        libvlc_video_set_aspect_ratio(media_player, "4:3");
    } else if (ar == Ratio16_9) {
        libvlc_video_set_aspect_ratio(media_player, "16:9");
    } else if (ar == Ratio16_10) {
        libvlc_video_set_aspect_ratio(media_player, "16:10");
    }
}

QMap<int, QString> Player::subtitles() const
{
    if (libvlc_video_get_spu_count(media_player) == 0) {
        return QMap<int, QString>();
    }

    QMap<int, QString> subs;
    libvlc_track_description_t *spu = libvlc_video_get_spu_description(media_player);
    libvlc_track_description_t *tmp = spu;

    while (tmp) {
        subs[tmp->i_id] = QString::fromUtf8(tmp->psz_name);
        tmp = tmp->p_next;
    }

    libvlc_track_description_list_release(spu);

    return subs;
}

int Player::currentSubtitle() const
{
    return libvlc_video_get_spu(media_player);
}

void Player::setSubtitle(int id)
{
    libvlc_video_set_spu(media_player, id);
}

QMap<int, QString> Player::audioTracks() const
{
    if (libvlc_audio_get_track_count(media_player) == 0) {
        return QMap<int, QString>();
    }
    
    QMap<int, QString> tracks;
    libvlc_track_description_t *track = libvlc_audio_get_track_description(media_player);
    libvlc_track_description_t *tmp = track;
    
    while (tmp) {
        tracks[tmp->i_id] = QString::fromUtf8(tmp->psz_name);
        tmp = tmp->p_next;
    }
    
    libvlc_track_description_list_release(track);
    return tracks;
}

int Player::currentAudioTrack() const
{
    return libvlc_audio_get_track(media_player);
}

void Player::setAudioTrack(int track)
{
    libvlc_audio_set_track(media_player, track);
}

void Player::onEventTriggered(int event)
{
    const auto vlcEvent = static_cast<libvlc_event_e>(event);

    switch (vlcEvent) {
        case libvlc_MediaPlayerMediaChanged:
            updateCurrentMedia();
            break;
        case libvlc_MediaPlayerPlaying:
        case libvlc_MediaPlayerPaused:
        case libvlc_MediaPlayerStopped:
        case libvlc_MediaPlayerNothingSpecial:
        case libvlc_MediaPlayerOpening:
        case libvlc_MediaPlayerBuffering:
        case libvlc_MediaPlayerEndReached:
        case libvlc_MediaPlayerEncounteredError:
            QMetaObject::invokeMethod(this, "stateChanged", Qt::QueuedConnection, Q_ARG(int, state()));
            updateCurrentMedia();
            break;
        case libvlc_MediaPlayerTimeChanged:
            QMetaObject::invokeMethod(this, "timeChanged", Qt::QueuedConnection, Q_ARG(qint64, time()));
        case libvlc_MediaPlayerPositionChanged:
            QMetaObject::invokeMethod(this, "positionChanged", Qt::QueuedConnection, Q_ARG(float, currentPosition()));
            break;
    };
}

void Player::eventAttach()
{
    if (!vlc)
        return;
    
    libvlc_event_manager_t *mpEventManager = libvlc_media_player_event_manager(media_player);
    
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerNothingSpecial , &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerOpening , &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerBuffering , &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerPlaying , &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerPaused, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerStopped, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerEndReached, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerEncounteredError, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerMediaChanged, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerTimeChanged, &event_callback, this);
    libvlc_event_attach(mpEventManager, libvlc_MediaPlayerPositionChanged, &event_callback, this);
}