#include "player.h"
#include "config.h"

#include <QDebug>

#include <vlc/vlc.h>

class PlayerPrivate {

public:
    
    libvlc_instance_t *vlcInstance = nullptr;
    
    libvlc_media_player_t *vlcPlayer = nullptr;
    
    libvlc_media_t *currentMedia = nullptr;
    
    libvlc_event_manager_t *eventManager = nullptr;
    
    libvlc_event_manager_t *mediaEventManager = nullptr;
    
    Player *player = nullptr;
    
    Player::PlayerState state = Player::Stopped;
    
    Player::MediaState mediaState = Player::EndReached;
    
    qint64 duration = 0;
    
    float position = 0.0f;
    
    qint64 time = 0;
    
    QUrl mediaUrl;

    bool isSeekable = false;
    
    int subtitlesCount = 0;
    
    int audioCount = 0;
    
    void onEventTriggered(const struct libvlc_event_t *);
    
    void onCurrentMediaChanged(struct libvlc_media_t *);
    
    void onLengthChanged(libvlc_time_t);
    
    void onTimeChanged(libvlc_time_t);
    
    void onMediaParseChanged(int);
    
    void onPositionChanged(float);
    
    void onStateChanged(Player::PlayerState);
    
    void onMediaStateChanged(Player::MediaState);
    
    void onSeekableChanged(bool);
    
};

static void event_callback(const struct libvlc_event_t *e, void *data)
{
    reinterpret_cast<PlayerPrivate*>(data)->onEventTriggered(e);
}


Player::Player(QObject *parent) : QObject(parent),
playerPrivate(std::make_unique<PlayerPrivate>())
{
    setupObjects();
}

Player::~Player()
{
    if (playerPrivate->vlcInstance) {
        libvlc_release(playerPrivate->vlcInstance);
    }
}

void Player::setupObjects()
{
    playerPrivate->player = this;
    
    playerPrivate->vlcInstance = libvlc_new(0, nullptr);
    
    libvlc_set_app_id(playerPrivate->vlcInstance, "io.gitlab.isseigx.nozomi", NOZOMI_VERSION, "nozomi");
    libvlc_set_user_agent(playerPrivate->vlcInstance, NOZOMI, NOZOMI_AGENT);
    
    playerPrivate->vlcPlayer = libvlc_media_player_new(playerPrivate->vlcInstance);
    libvlc_media_player_set_role(playerPrivate->vlcPlayer, libvlc_role_Video);
    
    playerPrivate->eventManager = libvlc_media_player_event_manager(playerPrivate->vlcPlayer);
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerOpening,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerBuffering,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerPlaying,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerPaused,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerStopped,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerEndReached,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerEncounteredError,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerMediaChanged,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerTimeChanged,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerPositionChanged,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerSeekableChanged,
                        &event_callback, playerPrivate.get());
    
    libvlc_event_attach(playerPrivate->eventManager, libvlc_MediaPlayerLengthChanged,
                        &event_callback, playerPrivate.get());
}

Player::PlayerState Player::state()
{
    return playerPrivate->state;
}

Player::MediaState Player::mediaState()
{
    return playerPrivate->mediaState;
}

float Player::currentPosition()
{
    return playerPrivate->position;
}

qint64 Player::currentDuration()
{
    return playerPrivate->duration;
}

qint64 Player::time()
{
    return playerPrivate->time;
}

void Player::play()
{
    if (state() == Playing) {
        libvlc_media_player_pause(playerPrivate->vlcPlayer);
    }
    else {
        libvlc_media_player_play(playerPrivate->vlcPlayer);
    }
}

void Player::stop()
{
    libvlc_media_player_stop(playerPrivate->vlcPlayer);
}

void Player::seek(float pos)
{
    if (!isSeekable()) {
        return;
    }
    
    libvlc_media_player_set_position(playerPrivate->vlcPlayer, pos);
    emit seeked(playerPrivate->time);
}

bool Player::isSeekable() const
{
    return playerPrivate->isSeekable;
}

void Player::setTime(qint64 t)
{
    if (!isSeekable()) {
        return;
    }
    
    libvlc_media_player_set_time(playerPrivate->vlcPlayer, (libvlc_time_t) t);
    emit seeked(playerPrivate->time);
}

void Player::setAspectRatio(AspectRatio ar)
{
    switch (ar) {
        case RatioDefault:
            libvlc_video_set_aspect_ratio(playerPrivate->vlcPlayer, 0);
            break;
        case Ratio4_3:
            libvlc_video_set_aspect_ratio(playerPrivate->vlcPlayer, "4:3");
            break;
        case Ratio16_9:
            libvlc_video_set_aspect_ratio(playerPrivate->vlcPlayer, "16:9");
            break;
        case Ratio16_10:
            libvlc_video_set_aspect_ratio(playerPrivate->vlcPlayer, "16:10");
            break;
    }
}

void Player::setCropVideo(AspectRatio ar)
{
    switch (ar) {
        case RatioDefault:
            libvlc_video_set_crop_geometry(playerPrivate->vlcPlayer, 0);
            break;
        case Ratio4_3:
            libvlc_video_set_crop_geometry(playerPrivate->vlcPlayer, "4:3");
            break;
        case Ratio16_9:
            libvlc_video_set_crop_geometry(playerPrivate->vlcPlayer, "16:9");
            break;
        case Ratio16_10:
            libvlc_video_set_crop_geometry(playerPrivate->vlcPlayer, "16:10");
            break;
    }
}

QMap<int, QString> Player::subtitles() const
{
    QMap<int, QString> subs;
    
    if (libvlc_video_get_spu_count(playerPrivate->vlcPlayer) == 0) {
        return subs;
    }
    
    auto spu = libvlc_video_get_spu_description(playerPrivate->vlcPlayer);
    auto tmp = spu;
    
    while (tmp) {
        subs[tmp->i_id] = QString::fromUtf8(tmp->psz_name);
        tmp = tmp->p_next;
    }
    
    libvlc_track_description_list_release(spu);
    
    return subs;
}

int Player::currentSubtitle() const
{
    return libvlc_video_get_spu(playerPrivate->vlcPlayer);
}

void Player::setSubtitle(int id)
{
    libvlc_video_set_spu(playerPrivate->vlcPlayer, id);
}

QMap<int, QString> Player::audioTracks() const
{
    QMap<int, QString> tracks;
    
    if (libvlc_audio_get_track_count(playerPrivate->vlcPlayer) == 0) {
        return tracks;
    }
    
    auto track = libvlc_audio_get_track_description(playerPrivate->vlcPlayer);
    auto tmp = track;
    
    while (tmp) {
        tracks[tmp->i_id] = QString::fromUtf8(tmp->psz_name);
        tmp = tmp->p_next;
    }
    
    libvlc_track_description_list_release(track);
    
    return tracks;
}

int Player::currentAudioTrack() const
{
    return libvlc_audio_get_track(playerPrivate->vlcPlayer);
}

void Player::setAudioTrack(int track)
{
    libvlc_audio_set_track(playerPrivate->vlcPlayer, track);
}

void* Player::engine() const
{
    return playerPrivate->vlcPlayer;
}

const QUrl& Player::media() const
{
    return playerPrivate->mediaUrl;
}

void Player::setMedia(const QUrl &media)
{
    if (media.isLocalFile()) {
        playerPrivate->currentMedia = libvlc_media_new_path(playerPrivate->vlcInstance,
                                                            media.toLocalFile().toUtf8().constData());
    } else {
        playerPrivate->currentMedia = libvlc_media_new_location(playerPrivate->vlcInstance,
                                                                media.toString().toUtf8().constData());
    }
    
    libvlc_media_player_set_media(playerPrivate->vlcPlayer, playerPrivate->currentMedia);
}

void Player::onStateChanged(PlayerState state)
{
    emit stateChanged(state);

    if (state != Player::Playing) {
        return;
    }
    
    int audioCount = libvlc_audio_get_track_count(playerPrivate->vlcPlayer);
    int subsCount = libvlc_video_get_spu_count(playerPrivate->vlcPlayer);

    if (audioCount != playerPrivate->audioCount) {
        playerPrivate->audioCount = audioCount;
        QMetaObject::invokeMethod(this, "audioTracksAvailableChanged", Qt::QueuedConnection);
    }
    
    if (subsCount != playerPrivate->subtitlesCount) {
        playerPrivate->subtitlesCount = subsCount;
        QMetaObject::invokeMethod(this, "subtitleAvailableChanged", Qt::QueuedConnection);
    }
}

QString Player::engineName() const
{
    return QStringLiteral("LibVLC");
}

QString Player::engineVersion() const
{
    return QString::fromUtf8(libvlc_get_version());
}

void Player::takeSnapshot(const QString &file, int width, int height)
{
    libvlc_video_take_snapshot(
        playerPrivate->vlcPlayer,
        0, file.toUtf8().constData(),
        width, height
    );
}

void Player::setAudioDelay(qint64 delay)
{
    libvlc_audio_set_delay(playerPrivate->vlcPlayer, delay);
}

qint64 Player::audioDelay() const
{
    return libvlc_audio_get_delay(playerPrivate->vlcPlayer);
}

void Player::setSubtitlesDelay(qint64 delay)
{
    libvlc_video_set_spu_delay(playerPrivate->vlcPlayer, delay);
}

qint64 Player::subtitlesDelay() const
{
    return libvlc_video_get_spu_delay(playerPrivate->vlcPlayer);
}

void PlayerPrivate::onEventTriggered(const struct libvlc_event_t *event)
{
    const auto vlcEvent = static_cast<libvlc_event_e>(event->type);
    
    switch (vlcEvent) {
        case libvlc_MediaPlayerMediaChanged:
            onCurrentMediaChanged(event->u.media_player_media_changed.new_media);
            break;
        case libvlc_MediaPlayerOpening:
            onMediaStateChanged(Player::Opening);
            break;
        case libvlc_MediaPlayerBuffering:
            onMediaStateChanged(Player::Buffering);
            break;
        case libvlc_MediaPlayerPlaying:
            onStateChanged(Player::Playing);
            break;
        case libvlc_MediaPlayerPaused:
            onStateChanged(Player::Paused);
            break;
        case libvlc_MediaPlayerStopped:
            onStateChanged(Player::Stopped);
            break;
        case libvlc_MediaPlayerEndReached:
            onMediaStateChanged(Player::EndReached);
            break;
        case libvlc_MediaPlayerEncounteredError:
            onMediaStateChanged(Player::EncounteredError);
            break;
        case libvlc_MediaPlayerTimeChanged:
            onTimeChanged(event->u.media_player_time_changed.new_time);
            break;
        case libvlc_MediaPlayerPositionChanged:
            onPositionChanged(event->u.media_player_position_changed.new_position);
            break;
        case libvlc_MediaPlayerSeekableChanged:
            onSeekableChanged(event->u.media_player_seekable_changed.new_seekable);
            break;
        case libvlc_MediaPlayerLengthChanged:
            onLengthChanged(event->u.media_player_length_changed.new_length);
            break;
        default:
            break;
    };
}

void PlayerPrivate::onCurrentMediaChanged(struct libvlc_media_t *media)
{
    const QString url = QUrl::fromPercentEncoding(libvlc_media_get_mrl(media));

    mediaUrl = {url};
    
    subtitlesCount = 0;
    audioCount = 0;
    
    QMetaObject::invokeMethod(player, "mediaChanged", Qt::QueuedConnection, Q_ARG(QUrl, mediaUrl));
}

void PlayerPrivate::onLengthChanged(libvlc_time_t length)
{
    auto duration_ = static_cast<qint64>(length);
    
    if (duration_ == duration) {
        return;
    }
    
    duration = duration_;
    
    QMetaObject::invokeMethod(player, "durationChanged", Qt::QueuedConnection, Q_ARG(qint64, duration));
}

void PlayerPrivate::onTimeChanged(libvlc_time_t t)
{
    const auto time64 = static_cast<qint64>(t);
    
    time = time64;
    
    QMetaObject::invokeMethod(player, "timeChanged", Qt::QueuedConnection, Q_ARG(qint64, time64));
}

void PlayerPrivate::onPositionChanged(float pos)
{
    position = pos;
    QMetaObject::invokeMethod(player, "positionChanged", Qt::QueuedConnection, Q_ARG(float, position));
}

void PlayerPrivate::onStateChanged(Player::PlayerState s)
{
    if (s == state) {
        return;
    }

    state = s;
    QMetaObject::invokeMethod(player, [this]() { player->onStateChanged(state); }, Qt::QueuedConnection);
}

void PlayerPrivate::onMediaStateChanged(Player::MediaState s)
{
    if (s == mediaState) {
        return;
    }

    mediaState = s;
    QMetaObject::invokeMethod(player, [this]() {
        emit player->mediaStateChanged(mediaState);
    }, Qt::QueuedConnection);
}

void PlayerPrivate::onSeekableChanged(bool seekable)
{
    isSeekable = seekable;

    QMetaObject::invokeMethod(player, "seekableChanged", Qt::QueuedConnection, Q_ARG(bool, isSeekable));
}

#include "moc_player.cpp"
