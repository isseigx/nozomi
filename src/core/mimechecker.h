#ifndef MIMECHECKER_H
#define MIMECHECKER_H

#include <QMimeDatabase>
#include <QMimeType>
#include <QObject>
#include <QString>
#include <QStringList>

namespace MimeChecker {

    static QStringList mimeTypes() {
        QStringList types({
            QStringLiteral("audio/aiff"),
            QStringLiteral("audio/vorbis"),
            QStringLiteral("audio/opus"),
            QStringLiteral("audio/x-opus+ogg"),
            QStringLiteral("audio/x-speex"),
            QStringLiteral("audio/flac"),
            QStringLiteral("audio/x-flac"),
            QStringLiteral("audio/x-realaudio"),
            QStringLiteral("audio/mpeg"),
            QStringLiteral("audio/aac"),
            QStringLiteral("audio/mp4"),
            QStringLiteral("audio/x-matroska"),
            QStringLiteral("audio/webm"),
            QStringLiteral("audio/3gpp"),
            QStringLiteral("audio/3ggp2"),
            QStringLiteral("audio/AMR"),
            QStringLiteral("audio/wav"),
            QStringLiteral("audio/ac3"),
            QStringLiteral("audio/midi"),
            QStringLiteral("audio/x-ape"),
            QStringLiteral("audio/x-musepack"),
            QStringLiteral("audio/x-wavpack"),
            QStringLiteral("audio/x-ms-wma"),
            QStringLiteral("video/ogg"),
            QStringLiteral("video/x-theora+ogg"),
            QStringLiteral("video/x-theora"),
            QStringLiteral("video/avi"),
            QStringLiteral("video/divx"),
            QStringLiteral("video/mpeg"),
            QStringLiteral("video/mp4"),
            QStringLiteral("video/x-m4v"),
            QStringLiteral("video/quicktime"),
            QStringLiteral("video/x-matroska"),
            QStringLiteral("video/webm"),
            QStringLiteral("video/3gp"),
            QStringLiteral("video/3gpp"),
            QStringLiteral("video/3gpp2"),
            QStringLiteral("video/flv"),
            QStringLiteral("video/x-flv"),
            QStringLiteral("video/x-ms-wmv"),
            QStringLiteral("video/x-mpeg2"),
            QStringLiteral("video/x-mpeg3"),
            QStringLiteral("video/mp4v-es")
        });
        
        return types;
    }
    
    static bool isSupported(const QString &file, const QStringList &types)
    {
        QMimeDatabase db;
        
        const auto mimeType = db.mimeTypeForFile(file);
        
        for (const auto &type : qAsConst(types)) {
            if (mimeType.inherits(type)) {
                return true;
            }
        }
        return false;
    }
    
    static bool isSupported(const QString &file)
    {
        return isSupported(file, mimeTypes());
    }
    
    static QString nameFilters()
    {
        return QObject::tr("Video files (*.3gp *.avi *.m4v *.mp4 *.mkv "
                "*.mov *.mpg *.mpeg *.ogg *.ogv *.rm *.webm *.wmv);;"
                "Audio files (*.3ga *.aac *.ac3 *.aiff *.amr *.ape "
                "*.flac *.m4a *.mid *.mka *.mp3 *.mpc *.mp+ *.mpp "
                "*.oga *.ogg *.opus *.ra *.wav *.wma *.mv)");
    }
}

#endif //MIMECHECKER_H