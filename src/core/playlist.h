#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QAbstractListModel>
#include <QUrl>

#include "player.h"

class Playlist : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    
public:
    explicit Playlist(Player *p, QObject *parent = nullptr);
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;
    
    Qt::DropActions supportedDropActions() const override;
    QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;


    void add(const QUrl &item);
    
    int currentIndex() const;
    
    void setCurrentIndex(int index);

public slots:
    void next();
    void previous();
    void clear();

signals:
    void currentIndexChanged(int idx);
    void finished();

private slots:
    void onMediaStateChanged(Player::MediaState state);
    void onStateChanged(Player::PlayerState state);
private:
    void connectToSignals();
    void setMedia();
    
    Player *mPlayer;
    QList<QUrl> mItems;
    QPersistentModelIndex mCurrentIndex;
};

#endif // PLAYLIST_H
