#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QMap>
#include <QUrl>

#include <memory>

class PlayerPrivate;

class Player : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PlayerState state READ state NOTIFY stateChanged)
    Q_PROPERTY(MediaState mediaState READ mediaState NOTIFY mediaStateChanged)
    Q_PROPERTY(float position READ currentPosition NOTIFY positionChanged)
    Q_PROPERTY(qint64 duration READ currentDuration NOTIFY durationChanged)
    Q_PROPERTY(qint64 time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QUrl media READ media WRITE setMedia NOTIFY mediaChanged)
    Q_PROPERTY(QString engineName READ engineName CONSTANT)
    Q_PROPERTY(QString engineVersion READ engineVersion CONSTANT)

public:
    explicit Player(QObject *parent = nullptr);
    ~Player();

    enum AspectRatio {
        RatioDefault,
        Ratio4_3,
        Ratio16_9,
        Ratio16_10
    };
    
    Q_ENUM(AspectRatio)
    
    enum PlayerState {
        Playing,
        Paused,
        Stopped
    };
    
    Q_ENUM(PlayerState);
    
    enum MediaState {
        Opening,
        Buffering,
        EndReached,
        EncounteredError
    };
    
    Q_ENUM(MediaState);

    PlayerState state();
    MediaState mediaState();
    float currentPosition();
    qint64 currentDuration();
    qint64 time();
    bool isSeekable() const;

    void setAspectRatio(AspectRatio);
    
    void setCropVideo(AspectRatio);

    // current subtitle id
    int currentSubtitle() const;
    
    // set subtitle by id
    void setSubtitle(int);
    
    // descriptions of subtitle tracks
    QMap<int, QString> subtitles() const;
    
    // descriptions for available audio tracks
    QMap<int, QString> audioTracks() const;
    
    // current audio track id
    int currentAudioTrack() const;
    
    // set audio track by id
    void setAudioTrack(int);
    
    void setMedia(const QUrl &);
    
    const QUrl& media() const;
    
    // return engine object
    void* engine() const;
    
    QString engineName() const;
    
    QString engineVersion() const;

    void setAudioDelay(qint64 delay);

    qint64 audioDelay() const;
    
    void setSubtitlesDelay(qint64 delay);
    
    qint64 subtitlesDelay() const;

public slots:
    void play();
    void stop();
    void seek(float);
    void setTime(qint64 t);
    void takeSnapshot(const QString &file, int width = 0, int height = 0);

signals:
    void stateChanged(Player::PlayerState state);
    void mediaStateChanged(Player::MediaState state);
    void positionChanged(float time);
    void durationChanged(qint64 time);
    void seeked(qint64 time);
    void timeChanged(qint64 time);
    void subtitleAvailableChanged();
    void audioTracksAvailableChanged();
    void mediaChanged(const QUrl &media);
    void seekableChanged(bool isSeekable);

private:
    void onStateChanged(PlayerState state);
    void setupObjects();
    
    friend class PlayerPrivate;
    
    std::unique_ptr<PlayerPrivate> playerPrivate;
};

#endif //PLAYER_H
