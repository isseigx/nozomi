#ifndef NOZOMI_H
#define NOZOMI_H

#include "core/player.h"
#include "core/playlist.h"
#include "ui/nozomimw.h"

#include <QObject>
#include <QPointer>
#include <QString>

class QTimer;
//class WaylandHelper;

class Nozomi : public QObject
{
    Q_OBJECT

public:
    Nozomi(QObject *parent = nullptr);
    ~Nozomi();

    void createPlayer();
    void createMainWindow();
    void sendFileToDBus(const QString &file);
    void initDBus();
    bool testDBusConnection() const;
    
    NozomiMW *mainWindow() const { return mainwindow_; }
    Player *player() const { return player_; }
    
    void playPause();
    void next();
    void prev();
    void stop();
    void toggleFullScreen();

private slots:
    void createWaylandHelper();
    
private:
    void setTheme();

    Playlist *m_playlist;
    QPointer<Player> player_;
    QPointer<NozomiMW> mainwindow_;
    
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    void createIdleTimer();
    QPointer<QTimer> idletimer;
#endif
    
    const QString SERVICE = QStringLiteral("org.mpris.MediaPlayer2.nozomi");
    const QString PATH = QStringLiteral("/org/mpris/MediaPlayer2");
    const QString INTERFACE = QStringLiteral("org.mpris.MediaPlayer2.Player");
};

#endif //NOZOMI_H
