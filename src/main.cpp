#include "nozomi.h"
#include "config.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("Nozomi"));
    app.setApplicationVersion(NOZOMI_VERSION);
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("nozomi"), QIcon(QStringLiteral(":/nozomi.svg"))));
    app.setOrganizationName(QStringLiteral("DreamState"));

    QCommandLineParser parser;
    parser.addPositionalArgument(QStringLiteral("file"),
        QApplication::translate("main", "File to play"));
    parser.setApplicationDescription(QApplication::translate("main", "Simple video player"));
    parser.addHelpOption();
    parser.addVersionOption();
    
    QCommandLineOption playop("play", QApplication::translate("main", "Play / Pause playback"));
    
    QCommandLineOption nextop("next", QApplication::translate("main", "Play next file in playlist"));
    
    QCommandLineOption prevop("prev", QApplication::translate("main", "Play previous file in playlist"));
    
    QCommandLineOption stopop("stop", QApplication::translate("main", "Stop playback"));
    
    QCommandLineOption togglefsop("fullscreen", QApplication::translate("main", "Toggle fullscreen"));
    
    parser.addOption(playop);
    parser.addOption(nextop);
    parser.addOption(prevop);
    parser.addOption(stopop);
    parser.addOption(togglefsop);
    
    parser.process(app);
    
    const QStringList args = parser.positionalArguments();

    Nozomi nozomi;
    
    if (parser.isSet(playop))
        nozomi.playPause();
    
    if (parser.isSet(nextop))
        nozomi.next();
    
    if (parser.isSet(prevop))
        nozomi.prev();
    
    if (parser.isSet(stopop))
        nozomi.stop();
    
    if (parser.isSet(togglefsop))
        nozomi.toggleFullScreen();
    
    if (nozomi.testDBusConnection()) {
        if (!args.isEmpty()) {
            nozomi.sendFileToDBus(args.first());
        }
        return 0;
    }
    
    nozomi.createPlayer();
    nozomi.createMainWindow();
    nozomi.initDBus();

    if (!args.isEmpty()) {
        nozomi.sendFileToDBus(args.first());
    }
    
    return app.exec();
}
