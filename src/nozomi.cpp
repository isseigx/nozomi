#include "nozomi.h"
#include "dbus/mediaplayer2.h"
#include "dbus/mediaplayer2player.h"
//#include "misc/waylandhelper.h"

#include <QApplication>
#include <QFile>
#include <QTimer>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDebug>

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
#include <KIdleTime>
#endif

Nozomi::Nozomi(QObject *parent) : QObject(parent)
{
}

Nozomi::~Nozomi()
{
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    if (idletimer) {
        idletimer->stop();
        delete idletimer;
    }
#endif
    delete mainwindow_;
}

void Nozomi::createPlayer()
{
    player_ = new Player(this);
    m_playlist = new Playlist(player_, this);
}

void Nozomi::createMainWindow()
{
    mainwindow_ = new NozomiMW(player_, m_playlist);
    mainwindow_->show();

    /*if (WaylandHelper::isWaylandSession()) {
        createWaylandHelper();
    } else {
        createIdleTimer();
        idletimer->start(60000);
    }*/
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    createIdleTimer();
    idletimer->start(60000);
#endif
}

void Nozomi::setTheme()
{
    QFile file(":/themes/less.qss");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    qApp->setStyleSheet(file.readAll());
}

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
void Nozomi::createIdleTimer()
{
    idletimer = new QTimer(this);

    connect(idletimer, &QTimer::timeout, this, [this]() {
        KIdleTime::instance()->simulateUserActivity();
    });
}
#endif

void Nozomi::initDBus()
{
    bool success = QDBusConnection::sessionBus().registerService(
        QStringLiteral("org.mpris.MediaPlayer2.nozomi"));
    
    if (success) {
        new MediaPlayer2(this);
        new MediaPlayer2Player(player_, m_playlist, this);

        QDBusConnection::sessionBus().registerObject(QStringLiteral("/org/mpris/MediaPlayer2"), 
        this, QDBusConnection::ExportAdaptors);
    }
}

bool Nozomi::testDBusConnection() const
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    return dbus.isValid();
}

void Nozomi::sendFileToDBus(const QString &file)
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    QDBusMessage msg = dbus.call(QStringLiteral("OpenUri"), file);
    
    if (msg.type() == QDBusMessage::ErrorMessage)
        qDebug() << tr("D-Bus Error: ") << msg.errorMessage();
}


void Nozomi::playPause()
{
    if (!testDBusConnection())
        return;
    
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("PlayPause"));
}

void Nozomi::next()
{
    if (!testDBusConnection())
        return;
    
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Next"));
}

void Nozomi::prev()
{
    if (!testDBusConnection())
        return;
    
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Previous"));
}

void Nozomi::stop()
{
    if (!testDBusConnection())
        return;
    
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Stop"));
}

void Nozomi::toggleFullScreen()
{
    if (!testDBusConnection())
        return;
    
    QDBusInterface dbus(SERVICE, PATH, QStringLiteral("org.mpris.MediaPlayer2"),
                        QDBusConnection::sessionBus());
    
    if (dbus.isValid()) {
        dbus.setProperty("FullScreen", QVariant(true));
    }
}

void Nozomi::createWaylandHelper()
{
    //wlhelper = new WaylandHelper(mainwindow_->winId());
}