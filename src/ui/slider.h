#ifndef SLIDER_H
#define SLIDER_H

#include <QSlider>

class Slider : public QSlider
{
    Q_OBJECT
    Q_DISABLE_COPY(Slider)

public:
    explicit Slider(QWidget *parent = nullptr);
    
    void mousePressEvent(QMouseEvent *ev) override;
};

#endif //SLIDER_H