#ifndef NOZOMIMW_H
#define NOZOMIMW_H

#include <QMainWindow>
#include <QTimer>

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
#include <KSelectAction>
#include <krecentfilesmenu.h>
#endif

#include "core/player.h"
#include "core/playlist.h"
#include "controlswidget.h"
#include "playlistview.h"
#include "ui_nozomimw.h"

class QActionGroup;
class QLabel;
class QSlider;

class NozomiMW : public QMainWindow
{
    Q_OBJECT
    Q_DISABLE_COPY(NozomiMW)

public:
    explicit NozomiMW(Player *p, Playlist *pl, QWidget *parent = nullptr);
    ~NozomiMW();

public slots:
    void toggleFullScreen();

protected:
    bool eventFilter(QObject *obj, QEvent *e) override;
    void closeEvent(QCloseEvent *e) override;
    void resizeEvent(QResizeEvent *e) override;

private slots:
    void hideCursor();
    void showCursor();
    void onAddFileRequested();
    void onAboutActionTriggered();
    void onAudioSyncActionTriggered();
    void onAudioTracksAvailableChanged();
    void onCurrentMediaChanged(const QUrl &media);
    void onMediaStateChanged(Player::MediaState state);
    void onStateChanged(Player::PlayerState state);
    void onSubtitleAvailableChanged();
    void onSubtitlesSyncActionTriggered();
    void onActionSnapshotTriggered();
    void open();

private:
    void connectActions();
    void createAudioMenu();
    void createCentralWidget();
    void createControlsWidget();
    void createDockWidget();
    void createToolBar();
    void createSlider();
    void createSubtitleMenu();
    void createVideoMenu();
    void enableActions();
    void disableActions();
    void setCurrentDirectory(const QString &file);

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    void createRecentFilesMenu();
    KRecentFilesMenu *m_recentFilesMenu;
    KSelectAction *m_audioTracksAction, *m_subtitlesAction;
#else
    QActionGroup *m_subtitleTracksActGroup, *m_audioTracksActGroup;
#endif

    Player *m_player;
    Playlist *m_playlist;
    PlaylistView *m_playlistview;
    
    QActionGroup *m_cropActGroup, *m_ratioActGroup;
    
    QLabel *m_durationLabel, *m_timeLabel;
    
    QSlider *m_seekslider;
    
    QWidget *m_centralwidget;
    ControlsWidget *mControlsWidget;
    
    QString m_currentDirectory;
    
    Ui_NozomiMW *ui;
    bool mIsCursorHidden = false;

    QCursor mOldCursor;
    QTimer *mCursorTimer;
};

#endif //NOZOMIMW_H