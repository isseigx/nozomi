#ifndef CONTROLSWIDGET_H
#define CONTROLSWIDGET_H

#include <QMenu>
#include <QWidget>

#include "core/player.h"
#include "core/playlist.h"
#include "ui_controlswidget.h"

class ControlsWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ControlsWidget)
    
public:
    explicit ControlsWidget(Player *player, Playlist *pl, QWidget *parent = nullptr);
    ~ControlsWidget();

    void addMenu(QMenu *menu);
    void resizeAndMove(const QSize &s);

signals:
    void exitFullScreen();

private:
    void connectToSignals();

    Player *mPlayer;
    Playlist *mPlaylist;
    Ui_ControlsWidget *ui;

    QMenu *mainMenu;
};

#endif //CONTROLSWIDGET_H