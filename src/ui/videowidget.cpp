#include "videowidget.h"

#include <QKeyEvent>

VideoWidget::VideoWidget(QWidget *parent) : QWidget(parent)
{
    setMouseTracking(true);
    setFocusPolicy(Qt::StrongFocus);
    
    mCursorTimer = new QTimer(this);
    mCursorTimer->setInterval(5000); // 5 seconds
    mCursorTimer->setSingleShot(true);
    connect(mCursorTimer, &QTimer::timeout, this, &VideoWidget::hideCursor);
}

VideoWidget::~VideoWidget()
{
}

// https://cgit.kde.org/kwidgetsaddons.git/tree/src/kcursor.cpp#n132
bool VideoWidget::event(QEvent *e)
{
    switch (e->type()) {
        case QEvent::Leave:
        case QEvent::FocusOut:
        case QEvent::WindowDeactivate:
            showCursor();
            break;
        case QEvent::KeyPress:
        case QEvent::ShortcutOverride:
            hideCursor();
            break;
        case QEvent::Enter:
        case QEvent::FocusIn:
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseButtonDblClick:
        case QEvent::MouseMove:
        case QEvent::Show:
        case QEvent::Hide:
        case QEvent::Wheel:
            showCursor();
            if (hasFocus()) {
                mCursorTimer->start();
            } else {
                setFocus();
            }
            break;
        default:
            break;
    }
    return QWidget::event(e);
}

void VideoWidget::showCursor()
{
    if (mCursorTimer->isActive()) {
        mCursorTimer->stop();
    }

    if (!mIsCursorHidden) {
        return;
    }

    mIsCursorHidden = false;

    if (cursor().shape() != Qt::BlankCursor) {
        return;
    }

    setCursor(mOldCursor);
    
    emit cursorShowed();
}

void VideoWidget::hideCursor()
{
    if (mCursorTimer->isActive()) {
        mCursorTimer->stop();
    }

    if (mIsCursorHidden) {
        return;
    }

    mIsCursorHidden = true;

    mOldCursor = cursor();
    setCursor(QCursor(Qt::BlankCursor));
    
    emit cursorHidden();
}

bool VideoWidget::isCursorHidden() const
{
    return mIsCursorHidden;
}
