#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractListModel>
#include <QUrl>

class Player;

class PlaylistModel : public QAbstractListModel
{
	Q_OBJECT

public:
    explicit PlaylistModel(Player *player, QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex&) const;
    Qt::DropActions supportedDropActions() const;
    Qt::ItemFlags flags(const QModelIndex &) const;
    QStringList mimeTypes() const;
    QMimeData* mimeData(const QModelIndexList &) const;
    bool canDropMimeData(const QMimeData *, Qt::DropAction, int, int, const QModelIndex &);
    bool dropMimeData(const QMimeData *, Qt::DropAction, int, int, const QModelIndex &);

signals:
    void dropFiles(int, const QList<QUrl> &);

private slots:
    void update();

private:
    void connectSignals();
    Player *m_player;
    QStringList m_itemslist;
};

#endif //PLAYLISTMODEL_H
