#include "slider.h"

#include <QMouseEvent>
#include <QStyle>

Slider::Slider(QWidget *parent) : QSlider(parent)
{
    setOrientation(Qt::Horizontal);
}

void Slider::mousePressEvent(QMouseEvent *ev)
{
    const int val = QStyle::sliderValueFromPosition(minimum(), maximum(),
                                                    ev->pos().x(), width());
    setValue(val);
    emit sliderMoved(val);
    QSlider::mousePressEvent(ev);
}
