#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QWidget>
#include <QEvent>
#include <QResizeEvent>
#include <QTimer>

#include "../core/player.h"

class VideoWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(VideoWidget)
    
public:
    explicit VideoWidget(QWidget *parent = nullptr);
    ~VideoWidget();

    bool isCursorHidden() const;

signals:
    void fullScreenMode();
    void cursorShowed();
    void cursorHidden();

protected:
    bool event(QEvent *e) override;

private slots:
    void hideCursor();
    void showCursor();

private:
    bool mIsCursorHidden = false;

    QCursor mOldCursor;
    QTimer *mCursorTimer;
};

#endif //VIDEOWIDGET_H
