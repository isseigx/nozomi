#include "playlistmodel.h"
#include "../core/player.h"

#include <QFileInfo>
#include <QMimeData>

PlaylistModel::PlaylistModel(Player *player, QObject *parent) : QAbstractListModel(parent),
m_player(player)
{
	connectSignals();
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_itemslist.size()) {
        return QVariant();
    }

    const QString &fileName = m_itemslist.at(index.row());

    if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
        return QFileInfo(fileName).fileName();
    }

    return QVariant();
}

int PlaylistModel::rowCount(const QModelIndex &parent = QModelIndex()) const
{
    return parent.isValid() ? 0 : m_itemslist.size();
}

void PlaylistModel::update()
{
    beginResetModel();
    endResetModel();
}

Qt::DropActions PlaylistModel::supportedDropActions() const
{
	return Qt::MoveAction | Qt::CopyAction;
}

Qt::ItemFlags PlaylistModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
         return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;

     return QAbstractListModel::flags(index) | Qt::ItemIsDragEnabled;
}

QStringList PlaylistModel::mimeTypes() const
{
	return QStringList() << "application/vnd.text.list" << "text/uri-list";
}

QMimeData* PlaylistModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    int row = (indexes.first().isValid() ? indexes.first().row() : 0);
    QByteArray encodedData(QByteArray::number(row));
    mimeData->setData(QStringLiteral("application/vnd.text.list"), encodedData);
    return mimeData;
}

bool PlaylistModel::canDropMimeData(const QMimeData *data,
                                    Qt::DropAction action, int row, int column,
                                    const QModelIndex &parent)
{
	Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(parent);
    Q_UNUSED(column);

    if (data->hasFormat(QStringLiteral("application/vnd.text.list")) || data->hasUrls())
        return true;

    return false;
}

bool PlaylistModel::dropMimeData(const QMimeData *data,
                                 Qt::DropAction action, int row, int column,
                                 const QModelIndex &parent)
{
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    if (action == Qt::IgnoreAction)
        return true;

    int to = 0;
    int count = m_player->itemsCount();

    if (!parent.isValid() && row < 0)
        to = count - 1;
    else if (!parent.isValid())
        to = qMin(row, (count - 1));
    else
        to = parent.row();

    if (data->hasUrls()) {
        emit dropFiles(row, data->urls());
        return false;
    }

    int oldRow = data->data(QStringLiteral("application/vnd.text.list")).toInt();
    m_player->moveItem(oldRow, to);

    return true;
}

void PlaylistModel::connectSignals()
{
    connect(m_player, &Player::itemMoved, [=](int from, int to) {
        beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
        m_itemslist.move(from, to);
        endMoveRows();
    });

    connect(m_player, &Player::itemsInserted, [=](int first, int last) {
        beginInsertRows(QModelIndex(), first, last);
        endInsertRows();
    });

    connect(m_player, &Player::itemRemoved, [=](int index) {
        beginRemoveRows(QModelIndex(), index, index);
        m_itemslist.removeAt(index);
        endRemoveRows();
    });

    connect(m_player, &Player::mediaAdded, [=](const QString &media) {
        m_itemslist << media;
    });

    connect(m_player, &Player::mediaInserted, [=](int index, const QString &media) {
        m_itemslist.insert(index, media);
    });
}
