#ifndef PLAYLISTVIEW_H
#define PLAYLISTVIEW_H

#include <QListView>

#include "core/player.h"
#include "core/playlist.h"

class QAction;

class PlaylistView : public QListView
{
    Q_OBJECT
    Q_DISABLE_COPY(PlaylistView)

public:
    PlaylistView(Playlist *pl, QWidget *parent = nullptr);
    ~PlaylistView();

signals:
    void addFileRequested();

private slots:
    void onCustomContextMenuRequested(const QPoint &);

private:
    void createActions();
    void connectToSignals();

    Playlist *m_playlist;
    QAction *m_addAction, *m_removeAction, *m_clearAction;
};

#endif //PLAYLISTVIEW_H