#include "config.h"
#include "nozomimw.h"
#include "slider.h"
#include "core/mimechecker.h"
#include "misc/utils.h"

#include <QActionGroup>
#include <QDebug>
#include <QDateTime>
#include <QEvent>
#include <QFileDialog>
#include <QFileInfo>
#include <QInputDialog>
#include <QLabel>
#include <QMessageBox>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QStandardPaths>

#include <vlc/vlc.h>

NozomiMW::NozomiMW(Player *player, Playlist *pl, QWidget *parent) : QMainWindow(parent),
m_player(player),
m_playlist(pl),
ui(new Ui_NozomiMW)
{
    ui->setupUi(this);
    mCursorTimer = new QTimer(this);
    mCursorTimer->setInterval(5000);
    mCursorTimer->setSingleShot(true);
    connect(mCursorTimer, &QTimer::timeout, this, &NozomiMW::hideCursor);
    
    m_currentDirectory = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation)
                        .constFirst();
    
    createCentralWidget();
    createVideoMenu();
    createAudioMenu();
    createSubtitleMenu();
    createSlider();
    createToolBar();
    createDockWidget();
    createControlsWidget();
    
    connectActions();
    
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    createRecentFilesMenu();
#endif
}

NozomiMW::~NozomiMW()
{
    delete ui;
}

void NozomiMW::connectActions()
{
    connect(m_player, &Player::mediaStateChanged, this, &NozomiMW::onMediaStateChanged);
    
    connect(m_player, &Player::stateChanged, this, &NozomiMW::onStateChanged);
    
    connect(m_player, &Player::subtitleAvailableChanged, this,
            &NozomiMW::onSubtitleAvailableChanged);
    
    connect(m_player, &Player::audioTracksAvailableChanged, this,
            &NozomiMW::onAudioTracksAvailableChanged);
    
    connect(m_player, &Player::mediaChanged, this, &NozomiMW::onCurrentMediaChanged);

    connect(m_player, &Player::positionChanged, this, [=](float pos) {
        m_seekslider->setValue(static_cast<int>(pos * 1000.0f));
    });
    
    connect(m_player, &Player::seekableChanged, this, [=](bool isSeekable) {
        m_seekslider->setEnabled(isSeekable);
    });
    
    connect(m_playlist, &Playlist::finished, this, [=]() {
        if (isFullScreen()) {
            toggleFullScreen();
        }
    });

    connect(m_playlistview, &PlaylistView::addFileRequested, this,
            &NozomiMW::onAddFileRequested);
    
    connect(m_seekslider, &QAbstractSlider::sliderMoved, this, [=](int value) {
        m_player->seek((float)value / 1000);
    });
    
    connect(m_player, &Player::timeChanged, this, [=](qint64 time) {
        m_timeLabel->setText(msecsToString(time));
    });
    
    connect(m_player, &Player::durationChanged, this, [=](qint64 duration) {
        m_durationLabel->setText(msecsToString(duration));
    });
    
    connect(ui->actionOpen, &QAction::triggered, this, &NozomiMW::open);
    
    connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
    
    connect(ui->actionAbout, &QAction::triggered, this, &NozomiMW::onAboutActionTriggered);
    
    connect(ui->actionSnapshot, &QAction::triggered, this,
            &NozomiMW::onActionSnapshotTriggered);

    connect(ui->actionAudioSync, &QAction::triggered, this,
            &NozomiMW::onAudioSyncActionTriggered);
    
    connect(ui->actionSubtitlesSync, &QAction::triggered, this,
            &NozomiMW::onSubtitlesSyncActionTriggered);
}

void NozomiMW::createCentralWidget()
{
    m_centralwidget = new QWidget(this);
    m_centralwidget->setMouseTracking(true);
    m_centralwidget->installEventFilter(this);
    setCentralWidget(m_centralwidget);
    
    libvlc_media_player_set_xwindow(reinterpret_cast<libvlc_media_player_t *>(m_player->engine()), m_centralwidget->winId());
    
    m_centralwidget->addAction(ui->actionPlay);
    m_centralwidget->addAction(ui->actionPrevious);
    m_centralwidget->addAction(ui->actionNext);
    m_centralwidget->addAction(ui->actionFullScreen);
}

void NozomiMW::open()
{       
    const QString file = QFileDialog::getOpenFileName(this, tr("Open files"),
                         m_currentDirectory, MimeChecker::nameFilters());
    
    if (file.isEmpty()) {
        return;
    }

    m_playlist->add(QUrl::fromLocalFile(file));
    m_playlist->setCurrentIndex(m_playlist->rowCount() - 1);
    setCurrentDirectory(file);
    
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    m_recentFilesMenu->addUrl(QUrl::fromLocalFile(file));
#endif
}

void NozomiMW::createAudioMenu()
{
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
    m_audioTracksActGroup = new QActionGroup(this);
    
    connect(m_audioTracksActGroup, &QActionGroup::triggered, this, [=](QAction *action) {
        m_player->setAudioTrack(action->data().toInt());
    });
#else
    ui->menuAudio->removeAction(ui->menuAudioTracks->menuAction());
    
    m_audioTracksAction = new KSelectAction(tr("Tracks"), this);
    m_audioTracksAction->setEnabled(false);
    
    connect(m_audioTracksAction, QOverload<QAction*>::of(&KSelectAction::triggered), this, [=](QAction *action) {
        m_player->setAudioTrack(action->data().toInt());
    });
    
    ui->menuAudio->insertAction(ui->actionAudioSync, m_audioTracksAction);
#endif
}

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
void NozomiMW::createRecentFilesMenu()
{
    m_recentFilesMenu = new KRecentFilesMenu(this);
    ui->menuFile->insertMenu(ui->actionExit, m_recentFilesMenu);
    
    connect(m_recentFilesMenu, &KRecentFilesMenu::urlTriggered, this, [=](const QUrl &url) {
        m_playlist->add(url);
        m_playlist->setCurrentIndex(m_playlist->rowCount() - 1);
    });
}
#endif

void NozomiMW::createSubtitleMenu()
{
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    m_subtitleTracksActGroup = new QActionGroup(this);
    
    connect(m_subtitleTracksActGroup, &QActionGroup::triggered, this, [=](QAction *action) {
        m_player->setSubtitle(action->data().toInt());
    });
#else
    ui->menuSubtitles->removeAction(ui->menuSubtitleTracks->menuAction());

    m_subtitlesAction = new KSelectAction(tr("Tracks"), this);
    m_subtitlesAction->setEnabled(false);
    
    connect(m_subtitlesAction, QOverload<QAction*>::of(&KSelectAction::triggered), this, [=](QAction *action) {
        m_player->setSubtitle(action->data().toInt());
    });
    
    ui->menuSubtitles->insertAction(ui->actionSubtitlesSync, m_subtitlesAction);
#endif
}

void NozomiMW::createVideoMenu()
{    
    ui->actionRatioDefault->setData(static_cast<int>(Player::RatioDefault));

    ui->actionRatio43->setData(static_cast<int>(Player::Ratio4_3));
    
    ui->actionRatio169->setData(static_cast<int>(Player::Ratio16_9));

    ui->actionRatio1610->setData(static_cast<int>(Player::Ratio16_10));
    
    m_ratioActGroup = new QActionGroup(this);
    m_ratioActGroup->addAction(ui->actionRatioDefault);
    m_ratioActGroup->addAction(ui->actionRatio43);
    m_ratioActGroup->addAction(ui->actionRatio169);
    m_ratioActGroup->addAction(ui->actionRatio1610);
    
    connect(m_ratioActGroup, &QActionGroup::triggered, this, [=](QAction *action) {
        m_player->setAspectRatio(static_cast<Player::AspectRatio>(action->data().toInt()));
    });
    
    
    ui->actionCropDefault->setData(static_cast<int>(Player::RatioDefault));

    ui->actionCrop43->setData(static_cast<int>(Player::Ratio4_3));
    
    ui->actionCrop169->setData(static_cast<int>(Player::Ratio16_9));

    ui->actionCrop1610->setData(static_cast<int>(Player::Ratio16_10));
    
    m_cropActGroup = new QActionGroup(this);
    m_cropActGroup->addAction(ui->actionCropDefault);
    m_cropActGroup->addAction(ui->actionCrop43);
    m_cropActGroup->addAction(ui->actionCrop169);
    m_cropActGroup->addAction(ui->actionCrop1610);
    
    connect(m_cropActGroup, &QActionGroup::triggered, this, [=](QAction *action) {
        m_player->setCropVideo(static_cast<Player::AspectRatio>(action->data().toInt()));
    });
    
}

void NozomiMW::createToolBar()
{
    connect(ui->actionPrevious, &QAction::triggered, m_playlist, &Playlist::previous);

    connect(ui->actionPlay, &QAction::triggered, m_playlist, [this]() {
        if (m_player->state() == Player::Stopped) {
            m_playlist->next();
        }
        else {
            m_player->play();
        }
    });

    connect(ui->actionStop, &QAction::triggered, m_player, &Player::stop);

    connect(ui->actionNext, &QAction::triggered, m_playlist, &Playlist::next);

    connect(ui->actionFullScreen, &QAction::triggered, this, &NozomiMW::toggleFullScreen);
    
    m_timeLabel = new QLabel(this);
    ui->toolBar->addWidget(m_timeLabel);
    ui->toolBar->addSeparator();

    ui->toolBar->addWidget(m_seekslider);
    ui->toolBar->addSeparator();
    
    m_durationLabel = new QLabel(this);
    ui->toolBar->addWidget(m_durationLabel);
}

void NozomiMW::createSlider()
{
    m_seekslider = new Slider();
    m_seekslider->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    m_seekslider->setMaximum(1000);
    m_seekslider->setEnabled(false);
}

void NozomiMW::onStateChanged(Player::PlayerState state)
{   
    if (state == Player::Playing) {
        ui->actionPlay->setIcon(QIcon::fromTheme(QStringLiteral("media-playback-pause"),
                                QIcon(QStringLiteral(":/pause.svg"))));
        m_durationLabel->setText(msecsToString(m_player->currentDuration()));
        enableActions();
    }
    else if (state == Player::Paused) {
        ui->actionPlay->setIcon(QIcon::fromTheme(QStringLiteral("media-playback-start"),
                                QIcon(QStringLiteral(":/play.svg"))));
    }
    else {
        ui->actionPlay->setIcon(QIcon::fromTheme(QStringLiteral("media-playback-start"),
                                QIcon(QStringLiteral(":/play.svg"))));
        setWindowTitle(QStringLiteral("Nozomi"));
        m_timeLabel->clear();
        m_durationLabel->clear();
        disableActions();
    }
}

void NozomiMW::onMediaStateChanged(Player::MediaState state)
{
    switch (state) {
        case Player::Opening:
        case Player::Buffering: {
            m_timeLabel->setText(QStringLiteral("--:--"));
            m_durationLabel->setText(QStringLiteral("--:--"));
            break;
        }
        default: {
            m_timeLabel->clear();
            m_durationLabel->clear();
            break;
        }
    }
}

void NozomiMW::onSubtitleAvailableChanged()
{   
    const QMap<int, QString> tracks = m_player->subtitles();

    if (tracks.isEmpty()) {
        return;
    }
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    ui->menuSubtitleTracks->setEnabled(true);
    ui->menuSubtitleTracks->clear();
#else
    m_subtitlesAction->setEnabled(true);
    m_subtitlesAction->clear();
#endif
    
    const int currentSub = m_player->currentSubtitle();

    QMapIterator<int, QString> it(tracks);
    while (it.hasNext()) {
        it.next();
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        QAction *action = ui->menuSubtitleTracks->addAction(it.value());
        action->setData(it.key());
        action->setCheckable(true);
        m_subtitleTracksActGroup->addAction(action);
        
        if (currentSub == it.key()) {
            action->setChecked(true);
        }
#else
        QAction *action = m_subtitlesAction->addAction(it.value());
        action->setData(it.key());
        
        if (currentSub == it.key()) {
            m_subtitlesAction->setCurrentAction(action);
        }
#endif
    }
}

void NozomiMW::onAudioTracksAvailableChanged()
{
    const QMap<int, QString> tracks = m_player->audioTracks();
    
    if (tracks.isEmpty()) {
        return;
    }

#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
    ui->menuAudioTracks->setEnabled(true);
    ui->menuAudioTracks->clear();
#else
    m_audioTracksAction->setEnabled(true);
    m_audioTracksAction->clear();
#endif

    const int currentTrack = m_player->currentAudioTrack();

    QMapIterator<int, QString> it(tracks);
    while (it.hasNext()) {
        it.next();
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
        QAction *action = ui->menuAudioTracks->addAction(it.value());
        action->setData(it.key());
        action->setCheckable(true);
        m_audioTracksActGroup->addAction(action);
        
        if (currentTrack == it.key()) {
            action->setChecked(true);
        }
#else
        QAction *action = m_audioTracksAction->addAction(it.value());
        action->setData(it.key());
        
        if (currentTrack == it.key()) {
            m_audioTracksAction->setCurrentAction(action);
        }
#endif
    }
}

void NozomiMW::toggleFullScreen()
{
    if (isFullScreen()) {
        menuBar()->show();
        ui->toolBar->show();
        mControlsWidget->hide();
    } else {
        menuBar()->hide();
        ui->toolBar->hide();

        if (!mIsCursorHidden) {
            mControlsWidget->show();
        }
    }
    setWindowState(windowState() ^ Qt::WindowFullScreen);
    ui->dockWidget->hide();
}

void NozomiMW::createDockWidget()
{
    m_playlistview = new PlaylistView(m_playlist, ui->dockWidget);
    ui->dockWidget->setWidget(m_playlistview);

    QAction *playlistAction = ui->dockWidget->toggleViewAction();
    playlistAction->setIcon(QIcon::fromTheme(QStringLiteral("view-media-playlist"),
                            QIcon(QStringLiteral(":/playlist.svg"))));

    playlistAction->setShortcut(tr("Shift+L"));
    ui->toolBar->addAction(playlistAction);

    ui->dockWidget->hide();
}

void NozomiMW::onAddFileRequested()
{
    const QStringList files = QFileDialog::getOpenFileNames(this, tr("Open files"),
                                m_currentDirectory, MimeChecker::nameFilters());
     
    for (const QString &file : qAsConst(files)) {
        m_playlist->add(QUrl::fromLocalFile(file));
    }
    
    if (!files.isEmpty()) {
        setCurrentDirectory(files.first());
    }
}

void NozomiMW::createControlsWidget()
{
    mControlsWidget = new ControlsWidget(m_player, m_playlist, this);
    mControlsWidget->hide();

    mControlsWidget->addMenu(ui->menuAudio);
    mControlsWidget->addMenu(ui->menuSubtitles);
    mControlsWidget->addMenu(ui->menuVideo);

    connect(mControlsWidget, &ControlsWidget::exitFullScreen, this, &NozomiMW::toggleFullScreen);
}

void NozomiMW::onCurrentMediaChanged(const QUrl &media)
{
    Q_UNUSED(media);

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    ui->menuSubtitleTracks->clear();
    ui->menuSubtitleTracks->setEnabled(false);
    
    ui->menuAudioTracks->clear();
    ui->menuAudioTracks->setEnabled(false);
#else
    m_subtitlesAction->clear();
    m_subtitlesAction->setEnabled(false);
    
    m_audioTracksAction->clear();
    m_audioTracksAction->setEnabled(false);
#endif
    
    setWindowTitle(QStringLiteral("%1 - Nozomi").arg(media.fileName()));
}

void NozomiMW::setCurrentDirectory(const QString &file)
{
    QFileInfo fileInfo(file);
    m_currentDirectory = fileInfo.canonicalPath();
}

void NozomiMW::onAboutActionTriggered()
{
    QMessageBox::about(this, tr("About %1").arg(NOZOMI),
                        tr("<h3>%1 %2</h3>"
                           "Simple video player<br>"
                           "Engine: %3 %4").arg(NOZOMI, NOZOMI_LONG_VERSION,
                                              m_player->engineName(),
                                              m_player->engineVersion()));
}

void NozomiMW::onActionSnapshotTriggered()
{
    const auto date = QDateTime::currentDateTime()
                        .toString(QStringLiteral("yyyy-MM-dd-hh-mm-ss-zzz"));
    const auto path = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation)
                        .constFirst();
    
    m_player->takeSnapshot(QStringLiteral("%1/nozomisnap-%2.png").arg(path, date));
}

void NozomiMW::onAudioSyncActionTriggered()
{
    bool ok;
    const auto delay = QInputDialog::getInt(this, tr("Audio Sync | Nozomi"),
                       tr("Enter negative (advance) or positive (delay) values, in milliseconds"),
                       m_player->audioDelay() / 1000, m_player->time() * -1, 
                       m_player->time(), 100, &ok);

    if (ok) {
        m_player->setAudioDelay(delay * 1000);
    }
}

void NozomiMW::onSubtitlesSyncActionTriggered()
{
    bool ok;
    const auto delay = QInputDialog::getInt(this, tr("Subtitles Sync | Nozomi"),
                                            tr("Enter negative (advance) or positive (delay) values, in milliseconds"),
                                            m_player->subtitlesDelay() / 1000,
                                            m_player->time() * -1, m_player->time(),
                                            100, &ok);
    
    if (ok) {
        m_player->setSubtitlesDelay(delay * 1000);
    }
}

bool NozomiMW::eventFilter(QObject *obj, QEvent *e)
{
    if (obj != m_centralwidget) {
        return false;
    }
    
    switch (e->type()) {
        case QEvent::Leave:
        case QEvent::FocusOut:
        case QEvent::WindowDeactivate:
            showCursor();
            return true;
        case QEvent::KeyPress:
        case QEvent::ShortcutOverride:
            hideCursor();
            return true;
        case QEvent::Enter:
        case QEvent::FocusIn:
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseMove:
        case QEvent::Show:
        case QEvent::Hide:
        case QEvent::Wheel:
            showCursor();
            if (m_centralwidget->hasFocus()) {
                mCursorTimer->start();
            } else {
                m_centralwidget->setFocus();
            }
            return true;
        case QEvent::MouseButtonDblClick:
            showCursor();
            toggleFullScreen();
            return true;
        default:
            return false;
    }
    return QObject::eventFilter(obj, e);
}

void NozomiMW::closeEvent(QCloseEvent *e)
{
    m_player->stop();
    QMainWindow::closeEvent(e);
}

void NozomiMW::resizeEvent(QResizeEvent *e)
{
    if (isFullScreen()) {
        mControlsWidget->resizeAndMove(e->size());
    }
}

void NozomiMW::showCursor()
{
    if (mCursorTimer->isActive()) {
        mCursorTimer->stop();
    }

    if (!mIsCursorHidden) {
        return;
    }

    mIsCursorHidden = false;

    if (cursor().shape() != Qt::BlankCursor) {
        return;
    }

    setCursor(mOldCursor);
    
    if (isFullScreen()) {
        mControlsWidget->show();
    }
}

void NozomiMW::hideCursor()
{
    if (mCursorTimer->isActive()) {
        mCursorTimer->stop();
    }

    if (m_player->state() == Player::Stopped) {
        return;
    }

    if (mIsCursorHidden) {
        return;
    }

    mIsCursorHidden = true;

    mOldCursor = cursor();
    setCursor(QCursor(Qt::BlankCursor));
    
    if (isFullScreen()) {
        mControlsWidget->hide();
    }
}

void NozomiMW::enableActions()
{
    ui->actionSnapshot->setEnabled(true);
    ui->actionFullScreen->setEnabled(true);
    ui->actionSubtitlesSync->setEnabled(true);
    ui->actionAudioSync->setEnabled(true);
    ui->actionNext->setEnabled(true);
    ui->actionPrevious->setEnabled(true);
}

void NozomiMW::disableActions()
{
    ui->actionSnapshot->setEnabled(false);
    ui->actionFullScreen->setEnabled(false);
    ui->actionSubtitlesSync->setEnabled(false);
    ui->actionAudioSync->setEnabled(false);
    ui->actionNext->setEnabled(false);
    ui->actionPrevious->setEnabled(false);
}
