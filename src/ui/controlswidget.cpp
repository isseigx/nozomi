#include "controlswidget.h"
#include "misc/utils.h"

ControlsWidget::ControlsWidget(Player *player, Playlist *pl, QWidget *parent) : QWidget(parent),
mPlayer(player),
mPlaylist(pl),
ui(new Ui_ControlsWidget)
{
    ui->setupUi(this);

    mainMenu = new QMenu(this);
    ui->mainMenuButton->setMenu(mainMenu);

    connectToSignals();
}

ControlsWidget::~ControlsWidget()
{
    delete ui;
}

void ControlsWidget::resizeAndMove(const QSize &s)
{
    resize(s.width(), size().height());
    move(0, s.height() - size().height());
}

void ControlsWidget::connectToSignals()
{
    connect(ui->previousButton, &QPushButton::clicked, mPlaylist, &Playlist::previous);
    
    connect(ui->playButton, &QPushButton::clicked, this, [=]() {
        if (mPlayer->state() == Player::Stopped) {
            mPlaylist->next();
        } else {
            mPlayer->play();
        }
    });
    
    connect(ui->nextButton, &QPushButton::clicked, mPlaylist, &Playlist::next);
    connect(ui->fullscreenButton, &QPushButton::clicked, this, &ControlsWidget::exitFullScreen);
    
    connect(ui->seekSlider, &QAbstractSlider::sliderMoved, this, [=](int value) {
        mPlayer->seek((float)value / 1000);
    });
    
    connect(mPlayer, &Player::positionChanged, this, [=](float pos) {
        ui->seekSlider->setValue(static_cast<int>(pos * 1000.0f));
    });
    
    connect(mPlayer, &Player::stateChanged, this, [=](Player::PlayerState state) {
        if (state == Player::Playing)
            ui->playButton->setIcon(QIcon::fromTheme(QStringLiteral("media-playback-pause")));
        else
            ui->playButton->setIcon(QIcon::fromTheme(QStringLiteral("media-playback-start")));
    });
    
    connect(mPlayer, &Player::timeChanged, this, [=](qint64 time) {
        ui->timeLabel->setText(msecsToString(time));
    });
    
    connect(mPlayer, &Player::durationChanged, this, [=](qint64 duration) {
        ui->durationLabel->setText(msecsToString(duration));
    });
}

void ControlsWidget::addMenu(QMenu *menu)
{
    mainMenu->addMenu(menu);
}
