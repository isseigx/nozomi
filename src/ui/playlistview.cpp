#include "playlistview.h"

#include <QAction>
#include <QFileInfo>
#include <QMenu>
#include <QUrl>

PlaylistView::PlaylistView(Playlist *pl, QWidget *parent) : QListView(parent),
m_playlist(pl)
{
    setModel(m_playlist);
    
    setContextMenuPolicy(Qt::CustomContextMenu);
    setDragEnabled(true);
    setDragDropMode(QAbstractItemView::DragDrop);
    setDefaultDropAction(Qt::MoveAction);
    
    createActions();
    connectToSignals();
}

PlaylistView::~PlaylistView()
{
}

void PlaylistView::connectToSignals()
{
    connect(this, &QListView::customContextMenuRequested, this, &PlaylistView::onCustomContextMenuRequested);
    
    connect(this, &QAbstractItemView::doubleClicked, this, [this](const QModelIndex &index) {
        m_playlist->setCurrentIndex(index.row());
    });
    
    connect(m_playlist, &Playlist::currentIndexChanged, this, [this](int idx) {
        setCurrentIndex(m_playlist->index(idx));
    });
}

void PlaylistView::createActions()
{
    m_addAction = new QAction(QIcon::fromTheme(QStringLiteral("list-add"),
                              QIcon(QStringLiteral(":/add.svg"))), tr("Add files"), this);
    connect(m_addAction, &QAction::triggered, this, &PlaylistView::addFileRequested);

    m_removeAction = new QAction(QIcon::fromTheme(QStringLiteral("list-remove"),
                                 QIcon(QStringLiteral(":/remove.svg"))), tr("Remove file from playlist"), this);
    
    connect(m_removeAction, &QAction::triggered, this, [=]() {
        m_playlist->removeRow(m_removeAction->data().toInt());
    });

    m_clearAction = new QAction(QIcon::fromTheme(QStringLiteral("list_remove-all")), tr("Clear playlist"), this);
    connect(m_clearAction, &QAction::triggered, m_playlist, &Playlist::clear);
}

void PlaylistView::onCustomContextMenuRequested(const QPoint &pos)
{
    const QModelIndex index = indexAt(pos);

    QMenu menu;
    menu.addAction(m_addAction);

    if (index.isValid()) {
        menu.addAction(m_removeAction);
        m_removeAction->setData(index.row());
    }

    if (m_playlist->rowCount({}) != 0)
        menu.addAction(m_clearAction);

    menu.exec(mapToGlobal(pos));
}
