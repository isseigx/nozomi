#ifndef MEDIAPLAYER2_H
#define MEDIAPLAYER2_H

#include <QDBusAbstractAdaptor>
#include <QStringList>

class Nozomi;

class MediaPlayer2 : QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2");
    
    Q_PROPERTY(bool CanRaise READ CanRaise);
    Q_PROPERTY(bool CanQuit READ CanQuit);
    Q_PROPERTY(bool CanSetFullScreen READ CanSetFullScreen);
    Q_PROPERTY(bool FullScreen READ FullScreen WRITE setFullScreen);
    Q_PROPERTY(bool HasTrackList READ HasTrackList);
    Q_PROPERTY(QString Identity READ Identity);
    Q_PROPERTY(QString DesktopEntry READ DesktopEntry);
    Q_PROPERTY(QStringList SupportedUriSchemes READ SupportedUriSchemes);
    Q_PROPERTY(QStringList SupportedMimeTypes READ SupportedMimeTypes);
    
public:
    explicit MediaPlayer2(QObject *parent);
    ~MediaPlayer2();
    
    bool CanRaise() const { return true; }
    bool CanQuit() const { return true; }
    bool CanSetFullScreen() const { return true; }
    bool HasTrackList() const { return false; }
    bool FullScreen() const;
    void setFullScreen(bool value) const;
    
    QString Identity() const;
    QString DesktopEntry() const;
    
    QStringList SupportedUriSchemes() const;
    QStringList SupportedMimeTypes() const;
    
public slots:
    Q_NOREPLY void Raise() const;
    Q_NOREPLY void Quit() const;
    
private:
    Nozomi *nozoapp;
    
};

#endif //MEDIAPLAYER_H
