#include "mediaplayer2player.h"

#include <QDBusConnection>
#include <QDBusMessage>

MediaPlayer2Player::MediaPlayer2Player(Player *p, Playlist *pl, QObject *parent) : QDBusAbstractAdaptor(parent),
m_player(p),
m_playlist(pl)
{
    connectActions();
}

MediaPlayer2Player::~MediaPlayer2Player()
{
}

QString MediaPlayer2Player::PlaybackStatus() const
{
    switch (m_player->state()) {
        case Player::Playing:
            return QStringLiteral("Playing");
        case Player::Paused:
            return QStringLiteral("Paused");
        default:
            return QStringLiteral("Stopped");
    }
}

QString MediaPlayer2Player::LoopStatus() const
{
    return QStringLiteral("None");
}

void MediaPlayer2Player::setLoopStatus(const QString &loopStatus) const
{
    Q_UNUSED(loopStatus)
}

double MediaPlayer2Player::Rate() const
{
    return 1.0;
}

void MediaPlayer2Player::setRate(double rate) const
{
    Q_UNUSED(rate)
}

bool MediaPlayer2Player::Shuffle() const
{
    return false;
}

void MediaPlayer2Player::setShuffle(bool shuffle) const
{
    Q_UNUSED(shuffle);
}

QVariantMap MediaPlayer2Player::Metadata() const
{
    QVariantMap metadata;

    metadata["xesam:title"] = m_player->media().fileName();
    return metadata;
}

double MediaPlayer2Player::Volume() const
{
    return 100.0;
}

void MediaPlayer2Player::setVolume(double volume) const
{
    Q_UNUSED(volume)
}

qlonglong MediaPlayer2Player::Position() const
{
    return m_player->time() * 1000;
}

double MediaPlayer2Player::MinimumRate() const
{
    return 1.0;
}

double MediaPlayer2Player::MaximumRate() const
{
    return 1.0;
}

bool MediaPlayer2Player::CanGoNext() const
{
    return true;
}

bool MediaPlayer2Player::CanGoPrevious() const
{
    return true;
}

bool MediaPlayer2Player::CanPlay() const
{
    return true;
}

bool MediaPlayer2Player::CanPause() const
{
    return true;
}

bool MediaPlayer2Player::CanControl() const
{
    return true;
}

bool MediaPlayer2Player::CanSeek() const
{
    return m_player->isSeekable();
}

void MediaPlayer2Player::Next()
{
    m_playlist->next();
}

void MediaPlayer2Player::Previous()
{
    m_playlist->previous();
}

void MediaPlayer2Player::Pause()
{
    m_player->play();
}

void MediaPlayer2Player::PlayPause()
{
    m_player->play();
}

void MediaPlayer2Player::Stop()
{
    m_player->stop();
}

void MediaPlayer2Player::Play()
{
    m_player->play();
}

void MediaPlayer2Player::Seek(qlonglong Offset) const
{
    m_player->seek(Offset / 1000);
}

void MediaPlayer2Player::SetPosition(const QDBusObjectPath& TrackId, qlonglong Position) const
{
    //TODO
    Q_UNUSED(TrackId)
    Q_UNUSED(Position)
}

void MediaPlayer2Player::OpenUri(const QString &Uri) const
{
    m_playlist->add(QUrl::fromLocalFile(Uri));
    int count = m_playlist->rowCount({});
    
    m_playlist->setCurrentIndex(count - 1);
}

void MediaPlayer2Player::currentMediaChanged()
{
    QVariantMap properties;
    properties["Metadata"] = Metadata();
    properties["CanSeek"] = CanSeek();

    propertiesChanged(properties);
}

void MediaPlayer2Player::playerState(Player::PlayerState state)
{
    Q_UNUSED(state);
    QVariantMap properties;
    properties["PlaybackStatus"] = PlaybackStatus();
    propertiesChanged(properties);
}

void MediaPlayer2Player::seekableChanged(bool isSeekable)
{
    QVariantMap properties;
    properties["CanSeek"] = isSeekable;
    propertiesChanged(properties);
}

void MediaPlayer2Player::propertiesChanged(const QVariantMap& properties) const
{
    QDBusMessage msg = QDBusMessage::createSignal(QStringLiteral("/org/mpris/MediaPlayer2"),
                                                  QStringLiteral("org.freedesktop.DBus.Properties"),
                                                  QStringLiteral("PropertiesChanged"));
    
    msg << QStringLiteral("org.mpris.MediaPlayer2.Player");
    msg << properties;
    msg << QStringList();
    
    QDBusConnection::sessionBus().send(msg);
}

void MediaPlayer2Player::connectActions()
{
    connect(m_player, &Player::mediaChanged, this, [=](const QUrl &media) {
        Q_UNUSED(media);
        currentMediaChanged();
    });

    connect(m_player, &Player::stateChanged, this, &MediaPlayer2Player::playerState);

    connect(m_player, &Player::seeked, this, [=](qint64 pos) {
        emit Seeked(pos * 1000);
    });

    connect(m_player, &Player::seekableChanged, this, &MediaPlayer2Player::seekableChanged);
}
