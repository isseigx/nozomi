#include "mediaplayer2.h"

#include "nozomi.h"
#include "core/mimechecker.h"

#include <QApplication>

MediaPlayer2::MediaPlayer2(QObject *parent) : QDBusAbstractAdaptor(parent)
{
    nozoapp = qobject_cast<Nozomi*>(parent);
}

MediaPlayer2::~MediaPlayer2()
{
    
}

QString MediaPlayer2::Identity() const
{
    return qApp->applicationName();
}

QString MediaPlayer2::DesktopEntry() const
{
    return QStringLiteral("jp.dreamstate.nozomi");
}

void MediaPlayer2::Raise() const
{
    nozoapp->mainWindow()->activateWindow();
    nozoapp->mainWindow()->raise();
}

bool MediaPlayer2::FullScreen() const
{
    return nozoapp->mainWindow()->isFullScreen();
}

void MediaPlayer2::setFullScreen(bool value) const
{
    Q_UNUSED(value);
    nozoapp->mainWindow()->toggleFullScreen();
}

void MediaPlayer2::Quit() const
{
    qApp->quit();
}

QStringList MediaPlayer2::SupportedUriSchemes() const
{
    return QStringList(QStringLiteral("file"));
}

QStringList MediaPlayer2::SupportedMimeTypes() const
{
    return MimeChecker::mimeTypes();
}
