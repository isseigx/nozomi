#ifndef THUMBNAIL_H
#define THUMBNAIL_H

#include <QString>

class Thumbnail
{
public:
    static QString generate(const QString &file);

private:
    Thumbnail();
};

#endif //THUMBNAIL_H
