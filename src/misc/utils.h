#ifndef UTILS_H
#define UTILS_H

#include <QString>

// convert milliseconds to time string
QString msecsToString(qint64 msecs);

#endif //UTILS_H
