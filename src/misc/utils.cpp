#include "utils.h"

#include <QTime>

QString msecsToString(qint64 msecs)
{
    QString format;
    
    if (msecs > 3600000) {
        format = QStringLiteral("h:mm:ss");
    }
    else {
        format = QStringLiteral("mm:ss");
    }
    
    return QTime::fromMSecsSinceStartOfDay(msecs).toString(format);
}
