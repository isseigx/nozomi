#ifndef WEBHANDLER
#define WEBHANDLER

#include <QObject>
#include <QIODevice>

class QProcess;

class WebHandler : public QObject
{
    Q_OBJECT
    
public:
    explicit WebHandler(QObject *parent = nullptr);
    ~WebHandler();
    
    static bool youtubedlAvailable();
    
public slots:
    void extract(const QString &);
    
signals:
    void error(const QString &errorStr);
    void finished(const QString &url);
    void isYouTube(const QString &video, const QString &audio);
    
private slots:
    void read();
    
private:
    void setupProcess();
    void mux(const QList<QByteArray> &);
    
    QProcess *youtubedl;
};

#endif //WEBHANDLER
