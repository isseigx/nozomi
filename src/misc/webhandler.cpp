#include "webhandler.h"

#include <QDebug>
#include <QProcess>

WebHandler::WebHandler(QObject *parent) : QObject(parent)
{
    setupProcess();
}

WebHandler::~WebHandler()
{
    delete youtubedl;
}

bool WebHandler::youtubedlAvailable()
{
    return QProcess::startDetached(QStringLiteral("youtube-dl --version"));
}

void WebHandler::setupProcess()
{
    youtubedl = new QProcess(this);
    youtubedl->setProgram(QStringLiteral("youtube-dl"));
    
    connect(youtubedl, &QProcess::readyReadStandardOutput, this, &WebHandler::read);
    
    connect(youtubedl, &QProcess::readyReadStandardError, [=]() {
        const QString output = QString::fromUtf8(youtubedl->readAllStandardError());
        emit error(output);
    });
}

void WebHandler::extract(const QString &url)
{
    youtubedl->setArguments(QStringList() << QStringLiteral("-g") << url);
    youtubedl->start();
}

void WebHandler::read()
{
    char s[] = "\n";
    QList<QByteArray> output = youtubedl->readAllStandardOutput().split(*s);
    QList<QByteArray> tmp;

    for (const QByteArray &item : output) {
        if (item.contains(QByteArrayLiteral("://"))) {
            tmp << item;
        }
    }
    
    if (tmp.isEmpty()) {
        emit error(tr("[WebHandler] Failed to find an URL"));
        return;
    }
    
    if (tmp.size() == 2) {
        mux(tmp);
    } else {
        emit finished(tmp.first());
    }
}

void WebHandler::mux(const QList<QByteArray> &urls)
{
    QString video, audio;
    
    for (const QByteArray &item : urls) {
        if (item.contains(QByteArrayLiteral("mime=video"))) {
            video = QString::fromUtf8(item);
            qDebug() << "[video]" << video;
        }
        if (item.contains(QByteArrayLiteral("mime=audio"))) {
            audio = QString::fromUtf8(item);
            qDebug() << "[audio]" << audio;
        }
    }
    
    if (!video.isEmpty() && !audio.isEmpty()) {
        emit isYouTube(video, audio);
    }
}
