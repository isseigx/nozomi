#ifndef WAYLANDHELPER_H
#define WAYLANDHELPER_H

#include <QPointer>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/idleinhibit.h>
#include <KWayland/Client/registry.h>
#include <KWayland/Client/surface.h>
#include <KWayland/Client/seat.h>
#include <KWayland/Client/pointer.h>

class WaylandHelper
{

public:
    WaylandHelper(WId id);
    ~WaylandHelper();
    
    static bool isWaylandSession();
    void hideCursor(bool);
    
private:
    void setupConnection();
    void createIdleInhibitor();
    void createPointerHandler(quint32, quint32);
    
    KWayland::Client::ConnectionThread *connection;
    KWayland::Client::Registry registry;
    KWayland::Client::IdleInhibitManager *idleinhibit;
    KWayland::Client::Surface *surface;
    KWayland::Client::Seat *seat;
    QPointer<KWayland::Client::Pointer> pointer;
    
    WId wid;
};

#endif //WAYLANDHELPER_H
