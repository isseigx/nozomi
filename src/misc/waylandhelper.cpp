#include "waylandhelper.h"
#include <QDebug>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/idleinhibit.h>
#include <KWayland/Client/registry.h>
#include <KWayland/Client/surface.h>

using namespace KWayland::Client;

WaylandHelper::WaylandHelper(WId id) : wid(id)
{
    setupConnection();
}

WaylandHelper::~WaylandHelper()
{
    surface->destroy();
    idleinhibit->destroy();
    registry.disconnect();
    connection->deleteLater();
}

void WaylandHelper::setupConnection()
{
    connection = ConnectionThread::fromApplication();
    
    if (!connection) {
        qDebug("Connection error");
        return;
    }
    
    registry.create(connection);
    registry.setup();
    
    surface = Surface::fromQtWinId(wid);
    
    QObject::connect(&registry, &Registry::idleInhibitManagerUnstableV1Announced, [=](quint32 name, quint32 version) {
        qDebug() << "[WaylandHelper]: Idle manager announced";
        createIdleInhibitor();
    });
    
    QObject::connect(&registry, &Registry::seatAnnounced, [=](quint32 name, quint32 version) {
        qDebug() << "[WaylandHelper]: Seat announced";
        createPointerHandler(name, version);
    });
}

bool WaylandHelper::isWaylandSession()
{
    return qgetenv("XDG_SESSION_TYPE") == QByteArrayLiteral("wayland");
}

void WaylandHelper::createIdleInhibitor()
{
    if (!registry.hasInterface(Registry::Interface::IdleInhibitManagerUnstableV1)) {
        qDebug("[WaylandHelper]: Idle inhibit is not supported");
        return;
    }
    
    auto interface = registry.interface(Registry::Interface::IdleInhibitManagerUnstableV1);
    
    idleinhibit = registry.createIdleInhibitManager(interface.name, interface.version);
    idleinhibit->createInhibitor(surface);
}

void WaylandHelper::createPointerHandler(quint32 name, quint32 version)
{
    seat = registry.createSeat(name, version);
    
    if (seat->isValid()) {
        pointer = seat->createPointer();
    }
}

void WaylandHelper::hideCursor(bool hide) {
    if (!pointer)
        return;
    
    if (hide)
        pointer->hideCursor();
    else
        pointer->setCursor(surface);
}
