#include "thumbnail.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QStandardPaths>

#include <libffmpegthumbnailer/videothumbnailer.h>

Thumbnail::Thumbnail()
{
}

QString Thumbnail::generate(const QString &file)
{
    if (!QFile::exists(file)) {
        return QString();
    }
    
    QString thumbfile = QStandardPaths::standardLocations(QStandardPaths::CacheLocation).first();
    
    QDir dir(thumbfile);
    if (!dir.exists()) {
        dir.mkpath(thumbfile);
    }
    
    QFileInfo fileInfo(file);
     
    thumbfile = QStringLiteral("%1/%2.jpg").arg(thumbfile, fileInfo.baseName());

    if (QFile::exists(thumbfile)) {
        return thumbfile;
    }

    ffmpegthumbnailer::VideoThumbnailer videoThumbnailer;
    videoThumbnailer.setThumbnailSize(256);
    videoThumbnailer.generateThumbnail(file.toStdString(), ThumbnailerImageType::Jpeg, thumbfile.toStdString());

    if (!QFile::exists(thumbfile)) {
        return QString();
    }
    
    return thumbfile;
}
